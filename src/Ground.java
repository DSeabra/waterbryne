import java.awt.Graphics2D;


public class Ground extends GMObject
{
    public Ground(int x, int y, int w, int h, int ix, int iy, boolean vis, boolean sol, int d)
    {
        super(x, y, w, h, ix, iy, vis, sol, d);
    }
    
    public Ground(int x, int y, int ix, int iy)
    {
        super(x, y, 64, 64, ix, iy, true, true, -100);
    }
    
    public void create()
    {        
        if (sprite == ImageCreator.images[0][9] || sprite == ImageCreator.images[0][8] || sprite == ImageCreator.images[0][7])
        {
            new Grass(x, y-64, 0, 1, false, true, -20);
        }
    }
    
    public void draw(Graphics2D g, Object drawer)
    {
        g.drawImage(sprite, x-Player.viewx, y, null);
    }
}
