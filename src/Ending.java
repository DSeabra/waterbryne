import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.util.ArrayList;


public class Ending extends GMObject
{
    private int ending;
    private int alpha, alpha2;
    private int count, counter;
    private int messagePos;
    private ArrayList<String> message = new ArrayList<String>();
    private boolean finishedTyping;
    
    public Ending(int e)
    {
        super(0, 0, (String) null, true, true, 100);
        ending = e;
    }
    
    public void start()
    {
        alpha = 0;
        count = 0;
        counter = 0;
        alpha2 = 0;
        finishedTyping = false;
    }
    
    public void step()
    {
        count++; 
        
        if (count > 120)
            alpha = Math.min(210, alpha + 1);
        
        if (alpha == 210)
            alpha2 = Math.min(255, alpha2 + 1);
        
        if (alpha2 == 255 && count % 2 == 0)
        {
            messagePos++;
            
            if (message.size() > 0 && counter < message.size() && messagePos >= message.get(counter).length())
                finishedTyping = true;
            else
                finishedTyping = false;
        }
        
        if (alpha2 == 255)
        {
            message.add("If what we dreamed had any consequence on the real world, no doubt it would be a much scarier"
                    + "place.");
            
            switch (ending)
            {
                case 0:
                    message.add("You: He..Hello?");
                    message.add("Woman: Honey?");
                    message.add("Man: Careful, he's just waking up. Sometimes memories are a bit clouded.");
                    message.add("Woman: Yes, doctor.");
                    message.add("Woman: You're awake! It's been a month!");
                    message.add("You: ...Jenn? What happened?");
                    message.add("Jenn: We were in a car crash. Remember?");
                    message.add("You: No. Yes. Yes, I do. Did the kids make it out okay?");
                    message.add("Jenn: Kids? What kids?");
                    message.add("Male: A slight amnesia is normal.");
                    message.add("Male: Don't worry, it'll fade.");
                    message.add("You: We didn't have kids?");
                    message.add("Jenn: No, we never did.");
                    message.add("You: Oh...I see.");
                    message.add("Doctor: You should get some more rest.");
                    message.add("You: Yes. Yes, I suppose I should.");
                    message.add("Your eyelids begin to fall over your eyes unwillingly.");
                    message.add("Before you close your eyes, you see some flowers next to you.");
                    message.add("Hastily scrawled on a notecard next to them you see three letters:");
                    message.add("An A, a C, and a backwards R.");
                    message.add("Doctor: Now that he's not in a coma, he shouldn't sleep long, ma'am.");
                    message.add("Jenn: Bye.");
                    message.add("You fall asleep.");
                    break;
                    
                case 1:
                    message.add("You: He..Hello?");
                    message.add("Man: Hello, sir.");
                    message.add("You: What...where...");
                    message.add("Man: Stay still. You were in a coma. Car accident. I'm your doctor.");
                    message.add("You: Oh. My family...");
                    message.add("Doctor: Yes, sir, we informed your parents and relatives.");
                    message.add("Doctor: You were only out for a three days, though.");
                    message.add("You: But...my wife? Kids?");
                    message.add("Doctor: Sir, you don't have a wife or kids. No girlfriend, either.");
                    message.add("Doctor: Here, lie down, you'll feel better after a rest.");
                    message.add("You: Oh...oh...ok, then.");
                    message.add("Doctor: And don't worry, sir, we'll let Asya know you're up.");
                    message.add("Your eyelids begin to fall over your eyes unwillingly.");
                    message.add("You fall asleep.");
                    break;
                    
                case 2:
                    message.add("You: He..Hello?");
                    message.add("You open your eyes, but no one is there.");
                    message.add("You (shout): HELLO?");
                    message.add("A man walks into the room. He seems astonished.");
                    message.add("Man: ...Impossible! You were in a coma for two years!");
                    message.add("You: What? Why?");
                    message.add("Man: Well, let me check your file. Car accident. How did you manage to wake up?");
                    message.add("You: ...I don't know. I had this crazy dream. I...was old. Very old.");
                    message.add("Man: Well, you go back to rest. I'll let your family know you're up.");
                    message.add("Man: Oh. What's this? You have a letter. From...uh...It's spelled A, C,"
                            + "and an R that's backwards?");
                    message.add("Man: You can read it when you wake up later.");
                    message.add("The man picks up the telephone and dials. You feel yourself nodding off.");
                    message.add("Man: Yes, hello? Is this Jenn? Your husband is awake...yes, of course.");
                    message.add("Man: Yes, it's a good idea to bring the kids. They'll help him recover.");
                    message.add("Your eyelids begin to fall over your eyes unwillingly.");
                    message.add("You fall asleep.");
                    break;
                    

            }
        }
    }
    
    public void draw(Graphics2D g, Object drawer)
    {
        g.setColor(new Color(255, 255, 255, alpha));
        g.fillRect(0, 0, 640, 480);
        
        g.drawImage(ImageCreator.images[0][0], 0, 258, null);
        
        if (ending == 0)
        {
            g.drawImage(ImageCreator.images[2][4], 256, 256 + 22, null);
            g.drawImage(ImageCreator.images[0][3], 64, 256 + 22, null);
        }
        
        else if (ending == 1)
        {
            g.drawImage(ImageCreator.images[2][4], 256, 256 + 22, null);  
        }
        
        else if (ending == 2)
        {
            g.drawImage(ImageCreator.images[2][4], 256, 256 + 22, null);
            g.drawImage(ImageCreator.images[0][3], 64, 256 + 22, null);
            g.drawImage(ImageCreator.images[2][3], 448, 256 + 22, null);
        }
        
        g.setColor(new Color(0, 0, 0, alpha2));
        g.fillRect(0, 0, 640, 480);
        
        g.setColor(Color.WHITE);
        g.setFont(new Font("Arial", Font.BOLD, 18));
        FontMetrics lolpaul = g.getFontMetrics();
        if (alpha2 == 255 && finishedTyping)
        {
            g.drawString("Press Space", (640-lolpaul.stringWidth("Press Enter"))/2, 400);
        }
        
        g.setFont(new Font("Arial", Font.BOLD, 12));
        
        if (message.size() != 0 && counter < message.size() && alpha2 == 255) {
            String message_TD = message.get(counter);
            String toDisplay = message_TD.substring(0, Math.min(message.get(counter).length(), messagePos));
            g.drawString(toDisplay, 32, 128);
        }
        else if (alpha2 == 255) {
            String message_TD = "             Thanks for playing.       The End.";
            int maxPos = Math.min("             Thanks for playing.       The End.".length(), messagePos);
            String toDisplay = message_TD.substring(0, maxPos);
            g.drawString(toDisplay, 32, 128);
        }
    }
    
    public void keyPressed(KeyEvent e)
    {
        if (e.getKeyCode() == KeyEvent.VK_SPACE && (finishedTyping || counter == 0))
        {
            counter++;
            messagePos = 0;
        }
    }
    

}
