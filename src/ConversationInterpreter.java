import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.util.ArrayList;


public class ConversationInterpreter extends GMObject
{
    static ArrayList<ArrayList<String>> lines;
    static int counter = 0;
    static boolean isConversing = false;
    private static int convNum = 0;
    static int canStart = 0;
    
    public ConversationInterpreter(ArrayList<ArrayList<String>> a)
    {
        super(0, 0, (String) null, false, true, 50);
        lines = a;
    }
    
    public static void startConversation(int convN)
    {
        if (canStart == 0)
        {
            Player.paused = true;
            isConversing = true;
            counter = 0;
            convNum = 99;
            
            for (int i = 0; i < lines.size(); i++)
            {
                ArrayList<String> a = lines.get(i);
                String lin = a.get(0);
                int convv = Integer.parseInt(lin.substring(lin.length()-3, lin.length()));
                
                if (Math.abs(convv - convN) <= 32)
                {
                    convNum = i;
                    break;
                }
            }
            
            if (convNum == 99)
            {
                endConversation();
            }
        }  
    }
    
    private static void endConversation()
    {
        Player.paused = false;
        isConversing = false;
        canStart = 60;
    }
    
    public void step()
    {
        canStart = Math.max(0, canStart-1);
    }
    
    public void keyPressed(KeyEvent e)
    {
        if (e.getKeyCode() == KeyEvent.VK_SPACE)
        {
            if (counter < lines.get(convNum).size()-1)
                counter++;
            else if (isConversing)
                endConversation();
        }
    }
    
    public void draw(Graphics2D g, Object drawer)
    {
        if (isConversing)
        {
            g.setColor(Color.BLACK);
            g.drawRect(2, 2, 635, 92);
            g.setColor(new Color(180, 180, 180));
            g.fillRect(3, 3, 634, 90);
            
            g.setColor(Color.BLACK);
            g.setFont(new Font("Arial", Font.PLAIN, 14));
            
            FontMetrics lolpaul = g.getFontMetrics();
            String properString = lines.get(convNum).get(counter);
            String[] splitSpc = properString.substring(2, properString.length()-3).split(" ");
            String[] talks = {"", "", ""};
            
            int i = 0;
            for (String s: splitSpc)
            {
                if (lolpaul.stringWidth(talks[i]) + lolpaul.stringWidth(s + " ") < 480)
                {
                    talks[i]+=s + " ";
                }
                else
                {
                    i++;
                    talks[i]+=s + " ";
                }
            }
            
            g.drawString(talks[0], 120, 30);
            g.drawString(talks[1], 120, 50);
            g.drawString(talks[2], 120, 70);
            
            if (Integer.parseInt(lines.get(convNum).get(counter).substring(1, 2)) < ImageCreator.portraits.length)
            {
                g.setColor(new Color(60, 53, 119));
                g.fillRect(8, 8, 104, 80);
                int location = Integer.parseInt(lines.get(convNum).get(counter).substring(1, 2));
                g.drawImage(ImageCreator.portraits[location], 10, 10, null);
            }
                
        }
    }

}
