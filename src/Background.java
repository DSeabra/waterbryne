import java.awt.Color;
import java.awt.Graphics2D;

public class Background extends GMObject {
    
    public Background()
    {
         super(0, 0, (String) null, false, false, -1000);
    }
    
    public void draw(Graphics2D g, Object drawer)
    {
        g.setColor(new Color(75, 66, 148));
        g.fillRect(0, 0, 640, 480);
    }

}
