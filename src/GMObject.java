/* Daniel Seabra de Andrade
 *
 *This will mimic a GM Object in Java it has the basic events and variables,
 * which are public like in Game Maker. It is up to the driver to call the
 * events appropriately.
 * */

import java.awt.Image;
import javax.swing.ImageIcon;
import java.util.ArrayList;

import java.awt.event.KeyEvent;
import java.awt.Rectangle;

import java.awt.Graphics;
import java.awt.Graphics2D;

public class GMObject
{
	public int x, y, w, h, depth;
	public String spriteloc;
	public Image sprite;
	public boolean solid, visible;
	public Rectangle bounds;
	public static ArrayList<GMObject> list;
	public boolean deactivated = false;
	public static String imageName;

	// PAULNOTE: Combine two constructors.
	public GMObject()
	{
		x = y = w = h = depth = 0;
		spriteloc = "";
		sprite = null;
		solid = false;
		visible = true;
		bounds = null;
		setConstruct();
		create();
	}

	public GMObject(int x, int y, String spriteloc, boolean solid, boolean visible, int depth)
	{
		this.x = x;
		this.y = y;
		this.spriteloc = spriteloc;
		this.solid = solid;
		this.visible = visible;
		this.depth = depth;

		if (spriteloc != null)
		{
			ImageIcon ii = new ImageIcon(this.getClass().getResource(spriteloc));
        	sprite = ii.getImage();
		}
		else
			sprite = null;

		setConstruct();
		create();
	}

	public GMObject(int x, int y, int imgX, int imgY, boolean solid, boolean visible, int depth)
	{
		this.x = x;
		this.y = y;
		this.solid = solid;
		this.visible = visible;
		this.depth = depth;

		sprite = ImageCreator.images[imgY][imgX];

		setConstruct();
		create();
	}

	public GMObject(int x, int y, int w, int h, String spriteloc, boolean solid, boolean visible, int depth)
	{
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.spriteloc = spriteloc;
		this.solid = solid;
		this.visible = visible;
		this.depth = depth;
		bounds = new Rectangle(x, y, w, h);

		if (spriteloc != null)
		{
			ImageIcon ii = new ImageIcon(this.getClass().getResource(spriteloc));
        	sprite = ii.getImage();
		}
		else
			sprite = null;

		setConstruct();
		create();
	}

	public GMObject(int x, int y, int w, int h, int imgX, int imgY, boolean solid, boolean visible, int depth)
	{
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.solid = solid;
		this.visible = visible;
		this.depth = depth;
		bounds = new Rectangle(x, y, w, h);

		sprite = ImageCreator.images[imgY][imgX];

		setConstruct();
		create();
	}

	// PAULNOTE: Don't keep in later versions. Instead combine the variable argument default parameter included with this.
	public final void setConstruct()
	{
		if (GMObject.list == null)
		{
			GMObject.list = new ArrayList<GMObject>();
			GMObject.list.add(this);
		}
		else
		{
			int lowerBound = 0;
			int upperBound = GMObject.list.size();

			while (true)
			{
				int pos = (lowerBound+upperBound)/2;
				int dep = GMObject.list.get(pos).depth;

				if (upperBound == pos || pos == lowerBound)
				{
					if (this.depth > dep)
						GMObject.list.add(lowerBound+1,this);
					else
						GMObject.list.add(lowerBound,this);
					break;
				}

				if (dep > this.depth)
				{
					upperBound = pos;
				}
				else if (dep < this.depth)
				{
					lowerBound = pos;
				}
				else
				{
					GMObject.list.add(pos,this);
					break;
				}
			}
		}
	}

	/* This sorts the array by depth, which is necessary to draw the objects in the right order.
     * Call this method or use this algorithm everytime a new object is created.
     * Note that this is not the most effective algorithm and could be replaced.
     */
    public final static void sort(ArrayList<GMObject> arraylist)
    {
    	for (int i=0; i<arraylist.size()-1; i++)
    	{
        	for (int j=i+1; j<arraylist.size(); j++)
        	{
        	    if (arraylist.get(i).depth < arraylist.get(j).depth)
        	    {
        	        GMObject temp = arraylist.get(i);
        	        arraylist.set(i, arraylist.get(j));
        	        arraylist.set(j, temp);
        	    }
        	}
        }
    }

    public final void setImage()
    {
		if (spriteloc != null)
		{
			ImageIcon ii = new ImageIcon(this.getClass().getResource(spriteloc));
        	sprite = ii.getImage();
		}
	}

	public final void setImage(int imgx, int imgy)
	{
		sprite = ImageCreator.images[imgy][imgx];
	}

	public final void updateRec()
	{
		if (bounds != null)
		{
			bounds = new Rectangle(x, y, w, h);
		}
	}

	public final void instanceDestroy()
	{
		for (int i = 0; i < list.size(); i++)
		{
			if (list.get(i) == this)
				list.remove(i);
		}
		destroy();
	}

	public void setDeac(boolean doyoureallywanttodothis)
	{
		deactivated = doyoureallywanttodothis;
	}


	public static final void destroyAll()
	{
		list = null;
	}

	public String toString()
	{
		return "X: " + x + "; Y: " + y + "; Sprite Location: " + spriteloc + "; Solid: " + solid + "; Visible: " + visible + "; Depth: " + depth;
	}

	public boolean isDeactivated()
	{
		return deactivated;
	}

	public void create() { }

	public void step() { }

	public void beginStep() { }

	public void endStep() { }

	public void deac() { }

	public void destroy() { }

	public void collision(Object x) { }

	public void draw(Graphics g, Object drawer) { }

	public void draw(Graphics2D g2, Object drawer) { }

	public void keyPressed(KeyEvent e) { }

	public void keyReleased(KeyEvent e) { }

}


