import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;


public class WorldCreator
{
    static WorldCreator world;
    
    public WorldCreator()
    {
        world = this;
        
        Scanner in = null;
        try {
            in = new Scanner(new File("level" + Player.level+".txt"));
        }
        catch (IOException e) {
            System.out.println("File not found, " + "level" + Player.level+".txt");
        }
        
        /*
         * Key of symbols
         * Format: Symbol - Object
         * 
         * Grounds:
         * 0 - Up-left
         * 1 - Up
         * 2 - Up-right
         * 3 - Right
         * 4 - Down-right
         * 5 - Down
         * 6 - Down-left
         * 7 - Left
         * 8 - Center
         * 
         * Scenery:
         * s - sign pointing right
         * b - (red/brown) bird
         * c - (cockatiel) bird
         * h - (humming) bird
         * g - game choice (sign)
         * 
         * ...
         * Space - Nothing
         */
        
        int lineNum = 0;
        String nL = " ";
        
        while (in.hasNextLine())
        {
            nL = in.nextLine();
            
            if (nL.length() > 0 && nL.substring(0, 1).equals("+"))
                break;
            
            for (int i = 0; i < nL.length(); i++)
            {
                char c = nL.toCharArray()[i];
                
                switch (c)
                {
                    case 48:
                        new Ground(i * 64, lineNum * 64, 7, 0);
                        break;
                    
                    case 49:
                        new Ground(i * 64, lineNum * 64, 8, 0);
                        break;
                        
                    case 50:
                        new Ground(i * 64, lineNum * 64, 9, 0);
                        break;
                    
                    case 51:
                        new Ground(i * 64, lineNum * 64, 9, 1);
                        break;
                    
                    case 52:
                        new Ground(i * 64, lineNum * 64, 9, 2);
                        break;
                        
                    case 53:
                        new Ground(i * 64, lineNum * 64, 8, 2);
                        break;
                        
                    case 54:
                        new Ground(i * 64, lineNum * 64, 7, 2);
                        break;
                    
                    case 55:
                        new Ground(i * 64, lineNum * 64, 7, 1);
                        break;
                        
                    case 56:
                        new Ground(i * 64, lineNum * 64, 8, 1);
                        break;
                        
                    case 115:
                        new RandomScenery(i * 64, lineNum * 64, 2, 0);
                        break;
                        
                    case 98:
                        new RandomScenery(i * 64, lineNum * 64 + 22, 3, 0);
                        break;
                        
                    case 99:
                        new RandomScenery(i * 64, lineNum * 64 + 22, 3, 2);
                        break;
                     
                    case 104:
                        new RandomScenery(i * 64, lineNum * 64 + 22, 4, 2);
                        break;
                    
                    case 103:
                        new RandomScenery(i * 64, lineNum * 64 + 4, 6, 0);
                        break;
                }
            }
            lineNum++;
        }
        
        ArrayList<ArrayList<String>> arrList = new ArrayList<ArrayList<String>>();
        int i = 0;
        arrList.add(new ArrayList<String>());
        
        while (true)
        {
            nL = in.nextLine();
            
            if (nL.length() > 0 && nL.substring(0, 1).equals(";"))
                break;
            
            if (nL.length() > 0 && nL.substring(0, 1).equals("+"))
            {
                i++;
                arrList.add(new ArrayList<String>());
                continue;
            }
            
            arrList.get(i).add(nL);
        }
        new ConversationInterpreter(arrList);
    }
    
    public void refreshWorld()
    {
        
    }
}
