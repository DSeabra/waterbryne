
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;



public class ImageCreator
{
	static BufferedImage[][] images;
	static BufferedImage bigImage;
	static BufferedImage[] flowers;
	static BufferedImage[] portraits;
	
	final int TILEWIDTH = 640;
	final int TILEHEIGHT = 480;


	public ImageCreator() throws IOException
	{
		String texturePackName = "images.png";
		Imager creator = new Imager(new File(texturePackName), 64, 64);

		bigImage = ImageIO.read(new File(texturePackName));
		images = new BufferedImage[TILEHEIGHT/64][TILEWIDTH/64];

		for (int x = 0; x < TILEWIDTH/64; x++)
		{
			for (int y = 0; y < TILEHEIGHT/64; y++)
			{
				BufferedImage thisImage = creator.getTile(x, y);
				images[y][x] = thisImage;
			}
		}
		
		flowers = new BufferedImage[3];
		for (int x = 0; x < 48; x+=16)
		{
		    flowers[x/16] = creator.getTile(64+x, 64, 16, 16);
		}
		
		int numPortraits = 6;
		portraits = new BufferedImage[numPortraits];
		
		for (int x = 0; x < 100*numPortraits; x += 100)
		{
		    portraits[x/100] = creator.getTile(x, 320, 100, 76);
		}
		
	}

}