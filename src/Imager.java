import java.awt.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;

public class Imager {
	public BufferedImage img;
	public int xsize, ysize;

	public Imager(File f) throws IOException {
		this(f, 0, 0);
	}

	public Imager(File f, int xx, int yy) throws IOException {
		img = ImageIO.read(f);
		setSize(xx, yy);
	}

	public void setSize(int xx, int yy) {
		xsize = xx;
		ysize = yy;
	}

	public BufferedImage getTile(int x, int y) {
		BufferedImage out = new BufferedImage(xsize, ysize, BufferedImage.TYPE_INT_ARGB);
		Graphics g = out.createGraphics();
		g.drawImage(img, -x*xsize, -y*ysize, null);
		g.dispose();

		return out;
	}

	public BufferedImage getTile(int x, int y, int w, int h) {
		BufferedImage out = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		Graphics g = out.createGraphics();
		g.drawImage(img, -x, -y, null);
		g.dispose();

		return out;
	}

	public BufferedImage getTexture() {
		return img;
	}
}
