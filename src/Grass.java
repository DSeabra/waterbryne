import java.awt.Graphics2D;


public class Grass extends GMObject {
    
    private int drawFlower;
    
    public Grass(int x, int y, int ix, int iy, boolean s, boolean vis, int d)
    {
        super(x, y, ix, iy, s, vis, d);
    }
    
    public void create()
    {
        int ranChance = (int) (Math.random()*11);
        
        drawFlower = ranChance;
    }
    
    public void draw(Graphics2D g, Object drawer)
    {   
        if (drawFlower < 3)
        {
            g.drawImage(ImageCreator.flowers[drawFlower], x-Player.viewx, y+52, null);
        }
        
        if (drawFlower > 3 && drawFlower < 6)
        {
            g.drawImage(ImageCreator.flowers[drawFlower-4], x+8-Player.viewx, y+52, null);
        }
        
        if (drawFlower > 6 && drawFlower < 9)
        {
            g.drawImage(ImageCreator.flowers[drawFlower-7], x+16-Player.viewx, y+52, null);
        }
        g.drawImage(sprite, x-Player.viewx, y, null);
    }
}
