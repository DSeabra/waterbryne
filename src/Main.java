//Daniel Seabra de Andrade
//This is the actual application itself, it just sets the basic things (size is here) and then calls Board.
//Most of the things here are self-explanatory.

import javax.swing.JFrame;


public class Main extends JFrame {

    /**
     * I really should figure out what that serialVersionUID thing is...
     */
    private static final long serialVersionUID = 1L;
    static JFrame frame;
    public static void main(String[] args)
	{
        frame = new Main();
        JFrame.setDefaultLookAndFeelDecorated(true);
    }

    public Main()
	{
        add(new Board());
        setUndecorated(true);
        setTitle("Waterbryne");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(640, 480);
        

        setLocationRelativeTo(null);
        setVisible(true);
        setResizable(false);
    }
}

