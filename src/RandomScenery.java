import java.awt.Graphics2D;

public class RandomScenery extends GMObject {

    private int animCounter;
    private boolean createBubble;
    
    public RandomScenery(int x, int y, int imgX, int imgY) {
        super(x, y, imgX, imgY, false, true, -80);
    }
    
    public RandomScenery(int x, int y, int imgX, int imgY, int d) {
        super(x, y, imgX, imgY, false, true, d);
    }
    
    public void create()
    {
        animCounter = 0;
        createBubble = true;
        
        // Birds create rocks
        if (sprite == ImageCreator.images[0][3] || sprite == ImageCreator.images[2][3] ||
                sprite == ImageCreator.images[2][4]) {
            new RandomScenery(x - 36, y+14, 4, 0);
        }
        
        // Rocks create the other rock part
        if (sprite == ImageCreator.images[0][4])
            new RandomScenery(x + 64, y, 5, 0);
    }
    
    public void step()
    {
        if (!Player.paused)
            animCounter++;
        
        if (animCounter % 60 == 0)
        {
            animCounter = 0;
            createBubble = true;
        }
        
        if (sprite == ImageCreator.images[1][3] && animCounter <= 30)
        {
            sprite = ImageCreator.images[0][3];
        }
        else if (sprite == ImageCreator.images[0][3] && animCounter > 30)
        {
            sprite = ImageCreator.images[1][3];
        }
        else if (sprite == ImageCreator.images[2][4] && animCounter > 30 && createBubble)
        {
            new RandomScenery(x+15, y-30, 5, 2);
            createBubble = false;
        }
        else if (sprite == ImageCreator.images[2][5] && animCounter > 30)
        {
            instanceDestroy();
        }
    }
    
    public void draw(Graphics2D g, Object drawer)
    {
        g.drawImage(sprite, x-Player.viewx, y, null);
    }

}
