// Daniel Seabra de Andrade
//
// This is the driver of the application.

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.Color;
import java.awt.Image;


import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import java.util.ArrayList;
import javax.swing.JPanel;

public class Board extends JPanel implements Runnable {

    /**
     * I have no idea what this serial version thing is.
     */
    private static final long serialVersionUID = 1L;

    // This is what makes it run every step.
    private Thread animator;

    // This is the ArrayList which holds all the GMObjects in the game
    static GMObject[] array = new GMObject[100000];

    // Delay in milliseconds. A delay of 20 will means that there will be 50 steps per second. (1000/20 = 50)
    static int DELAY = 15;

    // I wouldn't suggest changing much here. It fixes animation issues. This event is called only once. Put your initial objects in here.
    public Board() {
    addKeyListener(new TAdapter());
    setBackground(new Color(80,40,50));
        setFocusable(true);
        setDoubleBuffered(true);
        try{
            new ImageCreator();
        }
        catch (Exception e)
        {
            System.out.println("Error finding image file.");
        }
        new Player(0, 258, 32, 32, 0, 0, true, true, -60);
        new WorldCreator();
    }

    // This one starts the thread which makes the game run. Only called once.
    public void addNotify() {
        super.addNotify();
        animator = new Thread(this);
        animator.start();
    }

    // This calls the draw event of all the objects. It is necessary to copy the array in order to draw it (this is already done here).
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D)g;
        Image doubleBuff = createImage(640, 480);
        Graphics2D img = (Graphics2D) doubleBuff.getGraphics();

        for (GMObject xx: array)
        {
            xx.draw(img, this);
        }
        g2d.drawImage(doubleBuff, 0, 0, this);

        Toolkit.getDefaultToolkit().sync();
        g.dispose();
    }

    // This copies the array and is necessary.
    public static GMObject[] toArray(ArrayList<GMObject> one)
    {
        GMObject[] arr = new GMObject[one.size()];

        for (int i = 0; i < arr.length; i++)
            arr[i] = one.get(i);

        return arr;
    }

    // This class checks keys and sends the information to the objects.
    private class TAdapter extends KeyAdapter
    {
        public void keyReleased(KeyEvent e)
        {
            try
            {
                for (GMObject xx: GMObject.list)
                    xx.keyReleased(e);
            }
            catch (Exception f)
            {
                System.out.print("");
            }

        }

        public void keyPressed(KeyEvent e)
        {
            try
            {
                for (GMObject xx: GMObject.list)
                    xx.keyPressed(e);
            }
            catch (Exception f)
            {
                System.out.print("");
            }
        }

    }

    // This is the "Step" event of the entire game. In it, all the step events of the objects are called. Don't mess much with it.
    public void run()
    {
        array = toArray(GMObject.list);
        Thread.currentThread().setPriority(Thread.MIN_PRIORITY);

        while (true)
        {
            array = toArray(GMObject.list);

            for (GMObject x: array)
            {
                if (x.isDeactivated())
                    x.deac();

                if (!x.isDeactivated())
                    x.beginStep();
            }

            for (GMObject x: array)
            {
                if (x.isDeactivated())
                    x.deac();

                if (!x.isDeactivated())
                    x.step();
            }

            for (GMObject x: array)
            {
                if (x.isDeactivated())
                    x.deac();

                if (!x.isDeactivated())
                    x.updateRec();
            }

            for (GMObject x: array)
            {
                if (!x.isDeactivated())
                {
                    if (x.bounds!=null && x.solid==true)
                    {
                        for (GMObject y: array)
                        {
                            if (y.bounds!=null && y.solid==true)
                            {
                                if (x.bounds.intersects(y.bounds) && x!=y)
                                {
                                    x.collision(y);
                                }
                            }
                        }
                    }
                }
            }

            for (GMObject x: array)
            {
                if (x.isDeactivated())
                    x.deac();

                if (!x.isDeactivated())
                    x.endStep();
            }
            repaint();


            try
            {
                Thread.sleep(DELAY);
            }
            catch (InterruptedException e)
            {
                System.out.println("interrupted");
            }
        }
    }
}

