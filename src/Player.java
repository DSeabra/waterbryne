import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.util.ArrayList;


public class Player extends GMObject
{
    
    // Static variables
    static int viewx, viewy; // View positions
    static int level = 1;
    static boolean paused;
    
    // Public variables
    
    
    // Private variables
    private boolean[] keys; // An array that stores the pressed keys
    private double hspeed; // Horizontal speed
    private double airRes; // Air Resistance
    private int imageCounter; // For getting the right speed on images
    private String message;
    
    public Player(int x, int y, int w, int h, int imgX, int imgY, boolean solid, boolean visible, int depth)
    {
        super(x, y, w, h, imgX, imgY, solid, visible, depth);
    }
    
    public void create()
    {
        keys = new boolean[256];
        
        for (int i = 0; i < keys.length; i++)
            keys[i] = false;
        
        airRes = 0.2;
        hspeed = 0.01;
        imageCounter = 0;
        viewx = viewy = 0;
        paused = false;
        
        message = "";
        
        new Background();
        
    }
    
    public void step()
    {
        if (!Player.paused)
        {
            // Get the proper image
            getImage();
            
            // Endgame
            if (keys[KeyEvent.VK_ENTER])
            {
                if (level == 4)
                {
                    if (Math.abs(192 - x) < 32)
                    {
                        destroyAll();
                        Player p = new Player(x, y, 32, 32, 0, 0, true, true, -60);
                        p.x = 0;
                        Player.paused = true;
                        level++;
                        new WorldCreator();
                        new Ending(0);
                    }
                    
                    else if (Math.abs(384 - x) < 32)
                    {
                        destroyAll();
                        Player p = new Player(x, y, 32, 32, 0, 0, true, true, -60);
                        p.x = 0;
                        Player.paused = true;
                        level++;
                        new WorldCreator();
                        new Ending(1);
                    }
                        
                    else if (Math.abs(576 - x) < 32)
                    {
                        destroyAll();
                        Player p = new Player(x, y, 32, 32, 0, 0, true, true, -60);
                        p.x = 0;
                        Player.paused = true;
                        level++;
                        new WorldCreator();
                        new Ending(2);
                    }
                }
            }
            
            // Horizontal movement
            if (keys[KeyEvent.VK_RIGHT])
                hspeed+=1.5;
            
            if (keys[KeyEvent.VK_LEFT])
                hspeed-=1.5;
            
            if (keys[KeyEvent.VK_SPACE])
            {
                if(!paused && Math.abs(hspeed) < 0.7)
                    ConversationInterpreter.startConversation(x);
            }
            
            // Air resistance
            if (hspeed > 0)
                hspeed = hspeed - airRes*Math.pow(hspeed,2)*1.23*0.5*0.47;
            else
                hspeed = hspeed + airRes*Math.pow(hspeed,2)*1.23*0.5*0.47;
            
            
            x += (int) hspeed;
            if (level == 1 && x < 0)
            {
                x-=hspeed;
                x = 0;
            }
            else if (level == 4 && x > 576)
            {
                x-=hspeed;
                x = 576;
            }
            
            // Set the correct message
            message = "";
            
            for (int i = 0; i < ConversationInterpreter.lines.size(); i++)
            {
                ArrayList<String> a = ConversationInterpreter.lines.get(i);
                String lin = a.get(0);
                int convv = Integer.parseInt(lin.substring(lin.length()-3, lin.length()));
                
                if (Math.abs(convv - x) <= 32)
                {
                    message = "Press Space";
                    break;
                }
            }
            
            if (x > 645)
            {
                destroyAll();
                Player p = new Player(x, y, 32, 32, 0, 0, true, true, -60);
                p.x = -60;
                level++;
                new WorldCreator();
            }
            else if (x < - 60)
            {
                destroyAll();
                Player p = new Player(x, y, 32, 32, 0, 0, true, true, -60);
                p.x = 640;
                level--;
                new WorldCreator(); 
            }
        }
        
        // Exiting the game
        if (keys[KeyEvent.VK_ESCAPE] && !ConversationInterpreter.isConversing)
            System.exit(0);
    }
    
    public void draw(Graphics2D g, Object drawer)
    {
        g.drawImage(sprite, x, y, null);
        
        g.setColor(Color.WHITE);
        g.setFont(new Font("Arial", Font.PLAIN, 22));
        
        FontMetrics lolpaul = g.getFontMetrics();
        g.drawString(message, (640-lolpaul.stringWidth(message))/2, 400);
    }
    
    // Gets what the image should be this step
    public void getImage()
    {
        /* Little control structure, from highest priority to lowest
         * 0 - Jumping/Falling
         * 1 - Walking
         * 2 - Idle
         */
        
        // Not idle
        if (Math.abs(hspeed) > 0.9)
        {
            if (hspeed > 0)
                sprite = ImageCreator.images[4][imageCounter/8];
            else
                sprite = ImageCreator.images[3][imageCounter/8];
        }
        
        // Idle
        else
        {
            if (hspeed > 0)
                sprite = ImageCreator.images[0][0];
            else
                sprite = ImageCreator.images[0][1];
            imageCounter = 0;
        }
            
        imageCounter++;
        
        if (imageCounter % 48 == 0)
            imageCounter = 0;
    }
    
    // Key-handling stuff
    
    public void keyPressed(KeyEvent e)
    {
        keys[e.getKeyCode()] = true;
    }
    
    public void keyReleased(KeyEvent e)
    {
        keys[e.getKeyCode()] = false;
    }
    

}
