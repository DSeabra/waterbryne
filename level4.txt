



 c g  g  g
1111111111
8888888888
8888888888
+
-3Hello there! I'm Rainbow. What's your name?064
-0Well, I, uh, I kind of forgot, actually.064
-3Now that's odd! People are getting awfully forgetful these days. Do you know why these signs sort of appeared here a couple of seconds ago?064
-0Yeah, I did that. Apparently I really like signs. They are pretty nice, though. They don't have all these crazy emotions people have.064
-3I can understand that. People are oftentimes odd. Sometimes, I just want to forget that they exist.064
-0......
-3What? Did I say something wrong?064
-0No, you didn't. It's just...well, what you said. About forgetting. You see, I met these people, and I don't want to forget them.064
-3Then find a reason to remember them. You'll forget them if they are just 'these people.' But if they mean something to you, you'll remember. You don't forget who your family is, for instance.064
-0But they aren't my family.064
-3Well, obviously. Otherwise you wouldn't have trouble remembering who they are.064
-3What I meant was, make them mean something to you. Maybe they'll be so close to you that they'll be like your family sometime.064
-0But what if I can't really get to know them? What if I have to go and not see them for a long time?064
-3Are you sure you have to go? People are always in such a hurry that they don't stop to think that maybe they shouldn't be in a hurry sometimes.064
-0I'm not sure, but I'm going to be, soon. And I might have to go quickly.064
-3Well, if you have to go, and you musn't forget them, just make sure you have lots of reminders everywhere. If you think about them constantly, you probably won't forget them.064
-3Unless, of course, you completely forget that they ever existed, but ha, that doesn't happen.064
-0......
-3Did I misspeak again?064
-0No, no, nothing. I should go. I need to do something.064
-3Sure, go ahead. Good luck with your memory!064
+
-5Press enter here to choose option 1 - sail on the sea.192
+
-5Press enter here to choose option 2 - throw yourself off a cliff.384
+
-5Press enter here to choose option 3 - stay in Waterbryne.576
;